package curso;

// Importação do Pacote java.sql para acesso a bancos de dados relacionais
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Application {

	static Connection conection;

	public static void main(String[] args) {

		conection = conectaDB();

		inserirAlunos();

		consultaDisciplinas();

		consultaTodosAlunos();

		consultaUmAluno("777.999.999-99");

		System.out.println("");
		System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<< FIM >>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
	}

	public static Connection conectaDB() {


		System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<< CONEXÃO COM O BANCO DE DADOS  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");

		/* As classes Connection, Statement, PreparedStatement e ResultSet
		 * fazem parte do pacote java.sql e servem para acessar
		 * a persistencia de dados (banco de dados)
		 */

		// Dados necessários para CONECTAR AO BANCO DE DADOS
		String driver = "jdbc:mysql:";
		String servidor = "localhost";  // "localhost";
		String banco = "escola";
		String urlConexao = driver + "//" + servidor +  "/" + banco;
		String usuario = "root";
		String senha = "123";

		// <<<<<< CONEXÃO COM O DB >>>>>>
		Connection conexao = null;
		try {
			conexao = DriverManager.getConnection(urlConexao,usuario, senha);
			conexao.setAutoCommit(true);

			System.out.println(urlConexao);
			System.out.println("Conexão Realizada com sucesso !");

			return conexao;

		}
		catch(SQLException e ) {
			//e.printStackTrace();
			System.out.println("Erro de Conexão:" + e.getErrorCode() + " " + e.getMessage().toString());
			return null;
		}


	}

	public static void inserirAlunos() {


		System.out.println("");
		System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<< execução de INSERT >>>>>>>>>>>>>>>>>>>>>>>>>>>>>") ;


		// executa um comando DML (update ou insert)
		try {
			// instrução SQL a ser enviada ao BD
			Statement statement = conection.createStatement();

			statement.execute("insert into ALUNO (nome,cpf,matricula,idade,sexo) values ('João da Silva','001001003004','0000086',18,'M')");

			System.out.println("OK");

		} catch (SQLException e) {
			System.out.println("Erro de insert:" + e.getMessage().toString());
		}



		// executa um comando DML (update ou insert)
		/* Mais otimizado (rapida) para execuções repetidas
		 * e prática com métodos SET
		 */
		try {
			String sql = "";
			sql = "insert into ALUNO (nome,cpf,matricula,idade,sexo)";
			sql += " values (?,?,?,?,?)";
			PreparedStatement prepStatement = conection.prepareStatement(sql);

			prepStatement.setString(1, "Zezinho");
			prepStatement.setString(2, "1267");
			prepStatement.setString(3, "0000007");
			prepStatement.setInt(4, 15);
			prepStatement.setString(5, "M");

			prepStatement.execute();
			System.out.println("OK");

		} catch (SQLException e) {
			System.out.println("Erro de insert Prepared:" + e.getMessage().toString());
		}


	}


	public static void consultaDisciplinas() {

		// <<<<<<<<<<<<<<<<<<<<<<<<<<< INSTRUÇÕES SELECT  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>

		System.out.println("");
		System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<< execução de CONSULTA SQL Disciplinas  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		System.out.println("Consultando primeira e ultima disciplina...");

		try {
			Statement statement = conection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);

			/* a classe ResultSet respresenta uma matriz
               contendo as colunas e linhas resultantes
               da consulta select */
			ResultSet dbResultado = statement.executeQuery("Select id, titulo from DISCIPLINA order by titulo");

			// metodo que posiciona no primeiro registro da consulta
			dbResultado.first();
			System.out.println("Primeira Disciplina: " + dbResultado.getString("id") + " " + dbResultado.getString("titulo"));

			// metodo que posiciona no ultimo registro da consulta
			dbResultado.last();
			System.out.println("Ultima   Disciplina: " + dbResultado.getString("id") + " " + dbResultado.getString("titulo"));


		} catch (SQLException e) {

			System.out.println("Erro :" + e.getMessage().toString());
		}

	}




	public static void consultaTodosAlunos() {

		// <<<<<<<<<<<<<<<<<<<<<<<<<<< INSTRUÇÕES SELECT  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>

		System.out.println("");
		System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<< execução de CONSULTA SQL (com retorno Todos os Registros)  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>") ;

		try {

			Statement statement = conection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
			ResultSet dbResultado = statement.executeQuery("Select nome, cpf from ALUNO");


			System.out.println("Lista de Alunos");

			/*  o metodo first() posiciona no primeiro registro da consulta
			 *  e retorna false, se o resultado for vazio
			 */
			if (dbResultado.first()) {

				do {
					System.out.println("Nome: " + dbResultado.getString("nome") + " CPF: " + dbResultado.getString("cpf"));
				}
				while (dbResultado.next());

				/*  o metodo next() posiciona no próximo registro
				 *  da consulta.  Retorna false quando não houver
				 *  mais registros.
				 */
			}


		} catch (SQLException e) {

			System.out.println("Erro :" + e.getMessage().toString());
		}


	}


	public static void consultaUmAluno(String cpf) {

		// <<<<<<<<<<<<<<<<<<<<<<<<<<< INSTRUÇÕES SELECT  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>

		System.out.println("");
		System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<< execução de CONSULTA SQL (Um Aluno)  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>") ;

		try {

			PreparedStatement prepStatement = conection.prepareStatement("Select nome, cpf from ALUNO where cpf = ?",ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);

			prepStatement.setString(1, cpf);

			ResultSet dbResultado = prepStatement.executeQuery();

			if (dbResultado != null) {
				dbResultado.first();

				System.out.println("Nome: " + dbResultado.getString("nome") + " CPF: " + dbResultado.getString("cpf"));
			}

		} catch (SQLException e) {

			System.out.println("Erro :" + e.getMessage().toString());
		}


	}



}
